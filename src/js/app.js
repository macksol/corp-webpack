import React, { Component } from 'react';
import { render } from 'react-dom';
import Header from './components/header';
import Footer from './components/footer';
import treeImg from '../assets/tree.gif';

export default class Hello extends Component {
  render() {
    return (
        <div className="app">
            <Header/>
            <img src={treeImg} alt='tree house' style={{width:300}} />
            <Footer/>
        </div>
    );
  }
}

render(<Hello />, document.getElementById('app'));