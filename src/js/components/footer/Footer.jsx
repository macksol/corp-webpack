import React, { Component } from 'react';
import { render } from 'react-dom';
import footer from './footer.css';

export default class Footer extends Component {
  render() {
    return (
        <div className={footer.container}>Footer</div>
    );
  }
}