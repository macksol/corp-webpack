import React, { Component } from 'react';
import { render } from 'react-dom';
import header from './header.css';

export default class Header extends Component {
  render() {
    return (
        <div className={header.container}>Header</div>
    );
  }
}